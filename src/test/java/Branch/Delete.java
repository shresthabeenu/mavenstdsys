package Branch;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Delete {

	@Test
	public void methoddel(){
		
		System.setProperty("webdriver.chrome.driver", "drivers//chromedriver.exe");
		WebDriver driver=new ChromeDriver();  
		driver.manage().window().maximize();
		driver.get("http://wallbee.codebeestaging.com/");
		driver.findElement(By.id("UserName")).sendKeys("wallbeeadmin");
		driver.findElement(By.id("Password")).sendKeys("Wallbee123!");
		driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		
		//Click on Branch
		driver.findElement(By.xpath("//a[@class='Branch']")).click();
		
		//click on the delete icon
		driver.findElement(By.xpath("//tr[2]//td[2]//ul[1]//li[3]//a[1]//span[1]")).click();
		
		//Click on Continue button
		driver.findElement(By.xpath("//div[7]//div[3]//button[1]")).click();
		
}
}
