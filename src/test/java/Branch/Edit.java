package Branch;
	import java.util.concurrent.TimeUnit;
	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.testng.annotations.Test;

	
	public class Edit {
		
		@Test
	public void methodedit(){
			
			System.setProperty("webdriver.chrome.driver", "drivers//chromedriver.exe");
			WebDriver driver=new ChromeDriver();  
			driver.manage().window().maximize();
			driver.get("http://wallbee.codebeestaging.com/");
			driver.findElement(By.id("UserName")).sendKeys("wallbeeadmin");
			driver.findElement(By.id("Password")).sendKeys("Wallbee123!");
			driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
			driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
			
			//Click on Branch
			driver.findElement(By.xpath("//a[@class='Branch']")).click();
			
			//Click on Edit button
			driver.findElement(By.xpath("//tr[2]//td[2]//ul[1]//li[2]//a[1]//span[1]")).click();
			
			//Clear text
			driver.findElement(By.xpath("//input[@id='Route']")).clear();
			
			//Update Route
			driver.findElement(By.xpath("//input[@id='Route']")).sendKeys("2222");
			
			//Click on Save button
			driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
			
			}



	}
