package Branch;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;;

public class Create { 
	
	@Test
	public void methodCreate() {
	   
		System.setProperty("webdriver.chrome.driver", "drivers//chromedriver.exe");
		WebDriver driver=new ChromeDriver();  
		driver.manage().window().maximize();
		driver.get("http://wallbee.codebeestaging.com/");
		driver.findElement(By.id("UserName")).sendKeys("wallbeeadmin");
		driver.findElement(By.id("Password")).sendKeys("Wallbee123!");
		driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		
			//Click on Branch
			driver.findElement(By.xpath("//a[@class='Branch']")).click();
			
			//click on the Create button
			driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();
			
			//fill all the forms
			//Branch name
			driver.findElement(By.xpath("//input[@id='Name']")).sendKeys("ww");
			
			//Input Route
			driver.findElement(By.id("Route")).sendKeys("12");
					
			//Enter Serial
			driver.findElement(By.id("Serial")).sendKeys("123");
			
			//Enter Address
			driver.findElement(By.id("Address_Addressline1")).sendKeys("Kastrup");
			
			//Enter City
			driver.findElement(By.id("Address_City")).sendKeys("SLAGELSEVEJ 88");
			
			//Enter Email
			driver.findElement(By.id("Address_Email1")).sendKeys("beenu@codebee.dk");
			
			//Click on Create button
			driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();

				//Verify whether login is successful or not
				WebElement strValue = driver.findElement(By.xpath("//div[@class='page-title']"));
				String strExpected = "BRANCHES";
				String strActual = strValue.getText();
				
				
				
					if (strExpected.equals(strActual)) {
						
						System.out.println("Branch created successfully");
						
					} else {
						
						System.out.println("Branch creation failed");
						driver.findElement(By.xpath("/html[1]/body[1]/main[1]/form[1]/div[2]/div[1]/div[1]/input[1]")).clear();
						driver.findElement(By.xpath("//input[@id='Name']")).sendKeys("BeenuShrestha1");
						driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
						
					}
					
}
}


