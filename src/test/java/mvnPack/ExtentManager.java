package mvnPack;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import java.io.File;
import java.util.Date;

public class ExtentManager {
	
	public static ExtentReports getInstance() {
		
		Date date = new Date();
		
		System.out.println(date);
		
		
		String fileNameType = date.toString().replace(" ","_").replace(":", "_")+".html";
		
		String reportFilePath = "C:\\Users\\beenushrestha\\eclipse-workspace\\MavenStdSys\\reports\\" +fileNameType;
		
		ExtentReports eReports = new ExtentReports(reportFilePath, true, DisplayOrder.NEWEST_FIRST);
		
		File ReportsConfigFile = new File("ReportsConfig.xml");
		
		eReports.loadConfig(ReportsConfigFile);
		
		eReports.addSystemInfo("TestNg version", "7.1.0")
		.addSystemInfo("webdriver version", "3.13.0")
		.addSystemInfo("Environment", "QA")
		.addSystemInfo("Executed By", "Beenu")
		.addSystemInfo("Company Name", "Codebee Nepal")
		.addSystemInfo("Location", "Kathmandu");
		
		return eReports;
		
	}

}
