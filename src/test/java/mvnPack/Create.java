package mvnPack;

	import java.io.File;
	import java.io.FileInputStream;
	import java.io.IOException;
	import java.util.Properties;
	import java.util.concurrent.TimeUnit;
	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.chrome.ChromeDriver;

	import com.sun.tools.javac.util.Assert;

	public class Create {
	public static void main (String[] args) throws IOException {
			
			File propFile = new File ("Config//logindetails.properties");
			Properties prop = new Properties();
			FileInputStream fis = new FileInputStream(propFile);
			prop.load(fis);
			WebDriver driver = null;
			String browser = prop.getProperty("Chrome");
			System.setProperty("webdriver.chrome.driver", "drivers//chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get(prop.getProperty("url"));
			driver.findElement(By.id("UserName")).sendKeys(prop.getProperty("username"));
			driver.findElement(By.id("Password")).sendKeys(prop.getProperty("password"));;
			driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
			
			
			
			driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
			
			//Click on Company
			driver.findElement(By.xpath("//a[@class='Customer'][contains(text(),'Company')]")).click();
			
			//Select Company from the sub menu
			driver.findElement(By.xpath("//a[contains(text(),'Companies')]")).click();
			
			//Click on the Create button
			driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();
			
			//fill all the forms
			//Company name
			driver.findElement(By.xpath("//input[@id='Name']")).sendKeys("Codebee1");
	
			//Click on the branch drop down
			driver.findElement(By.xpath("//select[@id='BranchID']")).click();
					
			//Select branch
			driver.findElement(By.xpath("//option[contains(text(),'Wallbee-Branch')]]")).click();
			
			//Enter Address
			driver.findElement(By.id("Address_Addressline1")).sendKeys("Kastrup");
			
			//Enter City
			driver.findElement(By.id("Address_City")).sendKeys("SLAGELSEVEJ 88");
			
			//Enter Email
			driver.findElement(By.id("Address_Email1")).sendKeys("beenu@codebee.dk");
			
			//Click on Create button
			driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
			
			//assertion
			if(driver.getPageSource().contains("Wallbee1222")){
				System.out.println("Text is present");
				}else{
				System.out.println("Text is absent");
				}
			
			}

						
				


	}
