package mvnPack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Demo {
	
	ExtentReports eReport;
	ExtentTest eTest;
	
	@Test
	public void testOne() {
		
	    eReport = ExtentManager.getInstance();
		
		eTest = eReport.startTest("Test started here");
		
		System.setProperty("webdriver.chrome.driver", "drivers//chromedriver.exe");
	
		WebDriver driver = new ChromeDriver();
		
		eTest.log(LogStatus.INFO, "Chrome browser is opened");
		
		driver.manage().window().maximize();
		
		eTest.log(LogStatus.INFO, "Firefox browser got maximized");
		
		driver.get("http://www.omayo.blogspot.com");
		
		eTest.log(LogStatus.INFO, "Opened omayo application URL");
		
		driver.findElement(By.linkText("compendiumdev")).click();
		
		eTest.log(LogStatus.INFO, "Navigated to compendiumdev site after click");
		
		//driver.close();
		
		eTest.log(LogStatus.INFO, "Browser got closed");
		
		eTest.log(LogStatus.PASS, "Test one got passed");
		
	}
	
    @AfterMethod
    public void testClosure() {
    	
    	eReport.endTest(eTest);
    	
    	eReport.flush();
    	
    }
}
	
	